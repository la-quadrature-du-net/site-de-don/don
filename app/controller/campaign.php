<?php
namespace Controller;

use LQDN\Command\UserUpdateTotalCommand;
use LQDN\Command\UserCreateCommand;
use LQDN\Command\DonationCreateCommand;
use LQDN\Exception\InvalidEmailException;

class Campaign extends Controller
{
    /*
    Page principale du site
     */
    public static function show($f3, $args)
    {
        $f3->set('block_content', 'campaign/home.html');
    }

    // Page d'attente
    public function wait($f3)
    {
        $f3->set('block_content', 'campaign/wait.html');
    }

    // FAQ donateurs
    public function faq($f3)
    {
        $f3->set('block_content', 'campaign/faq.html');
    }

    public function merci($f3)
    {
        $f3->set('form_visible', 'merci');
        $f3->set('block_content', 'campaign/home.html');
    }

    // Don
    public function donate($f3, $args)
    {
        // First, let's validate that all the required data exists
        $sum = $f3->get('amount');
        if ($f3->get('amount_other') != '') {
            $sum = $f3->get('amount_other');
        }

        // If the donation is not a positive amount, we fail the donation.
        if ($f3->get('amount') < 1) {
            $f3->set("error", _("Montant du don invalide, il doit être supérieur à 1€."));
            $f3->error("403");
        }

        $f3->set('amount', $sum);

        $status = 0;
        $f3->set('monthly', $f3->get('monthly'));
        if ($f3->get('monthly') == "true") {
            $status = 100;
        }

        $cumul_id = 0;
        $db = $f3->get('DB');
        // Si l'utilisateur est déjà connecté, on le récupère
        if ($f3->get('SESSION.user', true)) {
            $user = $f3->get('container')['user_finder']->findById($f3->get('SESSION.id'));
            $email = $user['email'];
            $user_id = $user['id'];
            $cumul_id = $user['cumul'];
        } else {
            // Depuis les dons cumulés, on recherche d'abord si le donateur existe déjà (basé sur son email)
            $email = \Utils::asl($f3->get('email'));
            $hash = hash('sha256', $f3->get('password'));

            $user = $f3->get('container')['user_finder']->findByEmail(\Utils::asl($email));

            if (is_array($user) && count($user) > 0) {
                // We have an existing user, we should try to login with the provided password
                // or 403.
                $mapper = new \DB\SQL\Mapper($f3->get('DB'), 'users');
                $auth = new \Auth($mapper, array('id' => 'email', 'pw' => 'hash'));
                $login = $auth->login($email, $hash);
                if (!$login) {
                    $f3->error(403);
                }
                $email = $user['email'];
                $user_id = $user['id'];
                $cumul_id = $user['cumul'];
            } else {
                // The user does not exist, so let's create it
                try {
                    $f3->get('container')['command_handler']->handle(new UserCreateCommand($email, $hash, $f3->get('pseudo'), 0, 0));
                } catch (InvalidEmailException $e) {
                    $f3->set("error", _("Email Invalide"));
                    $f3->error("403");
                }

                $user = $f3->get('container')['user_finder']->findByEmail(\Utils::asl($email));
                $user_id = $user['id'];
                $user_log = new \Log('user.log');
                $user_log->write("ID ". $user_id. "\n");
            }
        }
        $sql = "INSERT INTO dons SET
			status = '".$status."',
			datec  = NOW(),
			somme  = '".$sum."',
			user_id  = '".$user_id."',
			public = '".intval($f3->get('public'))."',
			cumul  = '".intval($cumul_id)."';";

        $db->query($sql);
        $id = $db->lastInsertId();
        if (!$id) {
            //TODO: Test this part
            @mail(SYSADMIN, 'LQDN Don, bug Mysql');
            $errno = 7;
        }
        setcookie("donlqdn", md5("SALT!!!".$id."!!!"), 86400, "/");

        $target = PAYMENT_URL; //"https://paiement.systempay.fr/vads-payment/";
        $transaction_date = new \DateTime('now', new \DateTimeZone("UTC"));
        $params = array(
            // Champs obligatoires
            "vads_trans_date" => $transaction_date->format("YmdHis"),
            "vads_site_id" => SITE_ID,
            "vads_action_mode" => "INTERACTIVE",
            "vads_ctx_mode" => CTX_MODE,
            // Autres codes possibles (page 16)
            "vads_trans_id" => str_repeat("0", 6-strlen($id)).$id, // fill left zeros to have length 6
            "vads_version" => "V2",
            // Champs facultatifs
            "vads_language" => $f3->get('lang'),
            "vads_order_id" => $id,
            "vads_url_cancel" => ROOTURL,
            "vads_url_check" => RETURN_CHECK_URL,
            "vads_url_error" => ROOTURL,
            "vads_url_referral" => ROOTURL,
            "vads_url_refused" => ROOTURL,
            "vads_url_return" => ROOTURL . "merci",
            "vads_url_success" => ROOTURL,
            "vads_validation_mode" => "0",
            "vads_shop_name" => "La Quadrature du Net",
            "vads_shop_url" => ROOTURL
        );
        if ($f3->get('monthly') == "true") {
            // En cas de paiement récurrent, on doit créer un compte carte si ce n'est pas déjà fait
            $identifier = "";
            $identifier = $id . "_" . substr($email, 0, strpos($email, '@'));
            $db->query("UPDATE dons SET identifier = '".$identifier."' WHERE id = '".$id."'");
            $db->query("INSERT INTO identifiers (identifier, user_id) VALUES ('". $identifier ."','" .$user_id ."')");
            $params["vads_identifier"] = substr($identifier, 0, 50); // Pas plus de 50 caractères
            $params["vads_page_action"] = "REGISTER_SUBSCRIBE";
            $params["vads_cust_email"] = $email; // Email du porteur
            $params["vads_sub_effect_date"] = date("Ymd"); // Date d'effet à ce jour
            $params["vads_sub_amount"] = $sum*100;
            $params["vads_sub_currency"] = "978";
            $params["vads_sub_desc"] = "RRULE:FREQ=MONTHLY;BYMONTHDAY=7"; // Tous les 7 du mois
        } else {
            // En cas de paiement ponctuel, le montant est donné différemment
            $params["vads_page_action"] = "PAYMENT";
            $params["vads_amount"] = $sum*100;
            $params["vads_currency"] = "978";
            $params["vads_payment_config"] = "SINGLE";
        }
        // Calcul de la signature
        ksort($params);
        $signature = "";
        foreach ($params as $key=>$value) {
            $signature .= $value."+";
        }
        $signature .= CERTIFICATE;
        $signature = base64_encode(hash_hmac('sha256', $signature, CERTIFICATE, true));
        $params["signature"] = $signature;

        $f3->set('target', $target);
        $f3->set('vads_params', $params);
        $f3->set('signature', $signature);

        // Log des informations envoyées pour debug en cas de souci
        $don_log = new \Log('dons.log');
        $don_log->write('target : '. $target);
        $don_log->write('params : ');
        foreach ($params as $key=>$value) {
            $don_log->write($key.' : '.$value);
        }
        $don_log->write('certificate : '. CERTIFICATE);
        $don_log->write('signature : '. $signature);
        $f3->set('form_visible', 'vads');
        Campaign::show($f3, $args);
    }
};
