<?php
namespace Controller;

use LQDN\Command\UserUpdateCumulCommand;
use LQDN\Command\UserUpdateTotalCommand;
use LQDN\Command\DonationIncStatusCommand;

class Bank extends Controller
{
    /* We want to ignore the sanitizing on this page */
    public function sanitizeForms($f3)
    {
        return;
    }
    /*
    Page de retour de la banque
    */
    public function cb($f3, $args)
    {
        @include_once("config.php");

        $cb_log = new \Log('/cb.log');
        $error="";
        $result = $f3->get('POST.vads_result');
        $status = $f3->get('POST.vads_trans_status');
        $status_recurrence = $f3->get('POST.vads_recurrence_status');
        $status_identifier = $f3->get('POST.vads_identifier_status');
        $auth_result = $f3->get('POST.vads_auth_result');
        $extra_result = $f3->get('POST.vads_extra_result');
        $transaction_id = $f3->get('POST.vads_trans_id');
        $order_id = $f3->get('POST.vads_order_id');
        $version = $f3->get('POST.vads_version');
        $language = $f3->get('POST.vads_language');
        $currency = $f3->get('POST.vads_currency');
        $amount = $f3->get('POST.vads_amount');
        $sub_amount = $f3->get('POST.vads_sub_amount');
        $identifier = $f3->get('POST.vads_identifier');
        $certificate = $f3->get('POST.vads_payment_certificate');
        $warranty_result = $f3->get('POST.vads_warranty_result');
        $site_id = $f3->get('POST.vads_site_id');
        $contract_used = $f3->get('POST.vads_contract_used');
        $signature = $f3->get('POST.signature');

        $cb_log->write('result: '.$result);
        $cb_log->write('status: '.$status);
        $cb_log->write('status_recurrence: '.$status_recurrence);
        $cb_log->write('status_identifier: '.$status_identifier);
        $cb_log->write('extra_result: '.$extra_result);
        $cb_log->write('auth_result: '.$auth_result);
        $cb_log->write('signature: '.$signature);
        $cb_log->write('transaction_id: '.$transaction_id);
        $cb_log->write('order_id: '.$order_id);
        $cb_log->write('version: '.$version);
        $cb_log->write('language: '.$language);
        $cb_log->write('currency: '.$currency);
        $cb_log->write('amount: '.$amount);
        $cb_log->write('sub_amount: '.$sub_amount);
        $cb_log->write('identifier: '.$identifier);
        $cb_log->write('certificate: '.$certificate);
        $cb_log->write('warranty_result: '.$warranty_result);
        $cb_log->write('site_id: '.$site_id);
        $cb_log->write('contract_used: '.$contract_used);

        // Vérifications bancaires
        if ($site_id!=SITE_ID) {
            $error = "Bad site id: " . $site_id;
        }
        if ($version!="V2") {
            $error = "Bad return version: " . $version;
        }
        if ($status!='' && $contract_used!="5201306") {
            $error = "Bad contract used: " . $contract_used;
        }
        $vads_params = array();
        foreach ($_POST as $key => $value) {
            if (substr($key, 0, 4)=="vads") {
                $vads_params[$key] = $value;
            }
        }
        if (count($vads_params)>0) {
            ksort($vads_params);
        }
        $sig = "";
        foreach ($vads_params as $key => $value) {
            $sig .= $value . "+";
        }
        $sig .= CERTIFICATE;
        ### Attempt to do it in hmac-sha256
        $sig_hash = base64_encode(hash_hmac('sha256', $sig, CERTIFICATE, true));
        $cb_log->write("sig: " . $signature . " == " . $sig_hash);
        $cb_log->write("debug level: ". DEBUG);
        if ($sig_hash!=$signature and DEBUG == 0) {
            $error = "Error in signature: " . $signature . " != " . $sig_hash;
        }
        // Résultats des vérifications globales
        if ($error!="") {
            $cb_log->write($error);
            echo $error;
            exit(0);
        }

        /*
        Result: obligatoire pour savoir si tout est ok
        Status: optionnel
        Signature: obligatoire pour vérifier qu'il s'agit bien d'un POST de la part de la banque
        Transaction_id: obligatoire, correspond à l'id dans la base de données en interne
        Order_id: idem que Transaction_id sans les '0' devant
        Version: 'V2' obligatoirement
        Language: obligatoire pour faire le bon retour, sinon en EN
        Certificate: Très important en cas de souci, c'est l'identifiant côté banque
         */

        $db = $f3->get('DB');
        $id = intval($order_id);
        $cb_log->write("Id: ".$order_id);
        $don = $f3->get('container')['donation_finder']->findById($id);

        if (!$don) {
            $cb_log->write("Transaction id not found: ".$order_id);
            exit(0);
        }

        if ($don['status']!='0' && $don['status']!=100) {
            $cb_log->write("Maybe second call, actual status: ".$don['status']);
            exit(0);
        }

        if ($result!="00") {
            $cb_log->write("Payment failed for id: ".$order_id." with result: ".$result);
            $cb_log->write("Extra result: ".$extra_result);
            $cb_log->write("Auth result: ".$auth_result);
            exit(0);
        }

        // Set the lang environnement :
        $cb_log->write("System language : ".$f3->get('lang'));
        $lang = $don["lang"];
        $cb_log->write("User language : ".$lang);

        /* Language ok, set the locale environment */
        putenv("LC_MESSAGES=".$lang);
        putenv("LANG=".$lang);
        putenv("LANGUAGE=".$lang);
        // this locale MUST be selected in "dpkg-reconfigure locales"
        setlocale(LC_ALL, $lang);
        textdomain("messages");

        $charset="UTF-8";
        bind_textdomain_codeset("messages", "$charset");

        // ok, somme OK, status = completed, transaction found.
        $f3->get('container')['command_handler']->handle(new DonationIncStatusCommand($don['id']));
        $don = $f3->get('container')['donation_finder']->findById($don['id']);
        $status = $don['status'];

        $user = $f3->get('container')['user_finder']->findById($don['user_id']);
        $cb_log->write("Utilisation d'un utilisateur existant");
        //! FIXME What if user not found? Must check it
        // Ajout du nouveau don au cumul actuel
        if ($status!=101) {
            $cb_log->write("Ajout de ".$don['somme']);
            $f3->get('container')['command_handler']->handle(new UserUpdateTotalCommand($user['id'], (int) $user['total'] + $don['somme']));
            $f3->get('container')['command_handler']->handle(new UserUpdateCumulCommand($user['id'], (int) $user['cumul'] + $don['somme']));
        }
        $result = $db->query("SELECT cumul FROM users WHERE id='".$user['id']."'");
        $cumul = $result->fetch(\PDO::FETCH_ASSOC);
        $cumul = $cumul['cumul'];
        $cb_log->write("Nouveau cumul: ".$cumul);
        $user_id = $user['id'];
        // Ajout de son identifier dans la table prévue à cet effet si nécessaire
        if ($identifier!='') {
            $db->query("INSERT INTO identifiers (user_id, identifier) VALUES ('".$user_id."','".$identifier."');");
        }

        // Puis envoi du mail
        if ($user["email"]) {
            $mailer = new \SMTP(SMTP_HOST, SMTP_PORT, SMTP_SECURITY, SMTP_USER, SMTP_PW);
            $cb_log->write("Sending email for id: ".$id." at ".$user['email']);
            $mailer->set('From', "contact@laquadrature.net");
            $mailer->set('FromName', "La Quadrature du Net");
            $mailer->set('To', $user["email"]);
            $mailer->set('Subject', _("Merci de soutenir La Quadrature du Net !"));

            $mailer->set('CharSet', "UTF-8");
            // Préparation du texte du mail par morceaux
            $text = _("
Cher·e %%NAME%%,

Toute l'équipe de La Quadrature vous remercie du fond du cœur pour votre soutien !

Connectez-vous à votre page de donateur pour télécharger les mille décimales de π que nous vous adressons symboliquement pour votre soutien, et sélectionner les contreparties que vous souhaitez recevoir (en fonction de vos dons cumulés) : https://don.laquadrature.net/perso/ (identifiant = l'adresse mail utilisée pour faire votre don.)

Grâce à vous et aux bénévoles qui agissent au quotidien, La Quadrature pourra continuer à analyser les lois, prendre position, débattre, alerter, et mobiliser, afin qu'Internet reste un espace de liberté et de partage accessible à toutes et à tous.
Quelle que soit sa forme, la participation de chacun·e est indispensable pour les victoires citoyennes : n'hésitez pas à diffuser nos articles et nos actions auprès de votre famille, de vos collègues, de vos amis, et sur vos réseaux sociaux.
Agissons pour protéger nos droits fondamentaux, partager la culture, et nous réapproprier nos données !

Pour rester au courant de l'actualité sur ces questions, vous pouvez :
* nous suivre sur Twitter : https://twitter.com/laquadrature
* nous suivre sur Facebook : https://www.facebook.com/LaQuadrature
* nous suivre sur Mastodon : https://mamot.fr/@LaQuadrature
* vous abonner à notre lettre d'actualité hebdomadaire : https://lists.laquadrature.net/cgi-bin/mailman/listinfo/discussion
* vous abonner à notre newsletter mensuelle : https://lists.laquadrature.net/cgi-bin/mailman/listinfo/actu

Merci encore pour votre soutien !

Datalove <3

La Quadrature du Net
")."\n\n";

            // Création de l'url d'administration
            $admin_url = "https://don.laquadrature.net/perso";
            if ($f3->get('dev')) {
                $admin_url = "https://don.dev.laquadrature.net/perso";
            }

            // Remplacement des variables par leurs valeurs
            $fields = array(
                "NAME"=>$user["pseudo"],
                "SOMME"=>$don['somme'],
                "CUMUL"=>$cumul,
                "URL_ADMIN" => $admin_url
            );
            foreach ($fields as $k=>$v) {
                $text = str_replace("%%".$k."%%", $v, $text);
            }

            $sent = $mailer->send($text);
            if ($sent) {
                $cb_log->write("Send Mail OK");
            } else {
                $cb_log->write("Send Mail ERROR:" . $sent->log());
            }
        } else {
            $cb_log->write("Pas d'adresse mail");
        }

        echo "TOUT OK";
    }
};
