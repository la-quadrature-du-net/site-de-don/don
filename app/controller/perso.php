<?php
namespace Controller;

use LQDN\Command\AddressCreateCommand;
use LQDN\Command\AddressDeleteCommand;
use LQDN\Command\AddressUpdateCommand;
use LQDN\Command\CounterpartCreateCommand;
use LQDN\Command\CounterpartDeleteCommand;
use LQDN\Command\UserUpdateCumulCommand;
use LQDN\Command\UserUpdateTotalCommand;
use LQDN\Command\UserCreateCommand;

class Perso extends Controller
{
    /*
    Page principale de l'administration personnelle
     */
    public function beforeRoute($f3, $args)
    {
        parent::beforeRoute($f3, $args);
        $f3->clear('SESSION.form_visible');
    }

    public function show($f3, $args)
    {
        if (!$f3->exists('SESSION.user')) {
            $f3->reroute('/login');
        }
        $this->get_infos($f3, $args);
        $this->get_contreparties($f3);
        $this->get_dons($f3);
        $this->get_recurrents($f3);
        $this->compute_history($f3);
        if ($f3->exists('GET.validate')) {
            $f3->set('modal_counterparts', '1');
            $f3->clear('GET.validate');
        } else {
            $f3->set('modal_counterparts', '0');
        };
        $f3->set('block_content', 'user/perso.html');
    }

    public function get_infos($f3, $args)
    {
        // Chargement des informations de l'utilisateur
        $db = $f3->get('DB');
        $user = $f3->get('container')['user_finder']->findById($f3->get('SESSION.id'));
        $f3->set('infos', $user);
        // $result = $db->query("SELECT id, pdf, decimale FROM dons WHERE user_id='".\Utils::asl($user['id'])."' and pdf!='' ");
        $result = $db->query("SELECT id FROM dons WHERE user_id=" . \Utils::asl($user['id']));
        // $pdfs = array();
        // foreach ($result->fetchAll(\PDO::FETCH_ASSOC) as $row) {
        //     $pdfs[$row['pdf']] = $row;
        // }
        $addresse = $f3->get('container')['address_finder']->findByUserId((int) $user['id']);
        $f3->set('adresse', $addresse);
        // $f3->set('pdfs', $pdfs);
    }

    public function get_contreparties($f3)
    {
        // Chargement des contreparties de l'utilisateur
        $contreparties = $f3->get('container')['counterpart_finder']->findByUserId((int) $f3->get('SESSION.id'));
        $f3->set('contreparties', $contreparties);
    }

    public function get_dons($f3)
    {
        // Chargement des dons de l'utilisateur
        $db = $f3->get('DB');
        $dons = $f3->get('container')['donation_finder']->findByUserId((int) $f3->get('SESSION.id'));
        $dons = array_filter($dons, function ($don) {
            return in_array((int) $don['status'], [1, 4]);
        });
        $f3->set('dons', $dons);
    }

    public function get_recurrents($f3)
    {
        // Chargement des abonnements de l'utilisateur
        $db = $f3->get('DB');
        $dons = $f3->get('container')['donation_finder']->findByUserId((int) $f3->get('SESSION.id'));
        $dons = array_filter($dons, function ($don) {
            return in_array((int) $don['status'], [101, 102]);
        });
        $f3->set('dons_recurrents', $dons);
    }

    public function compute_history($f3)
    {
        $history = array();
        $history = array_merge($f3->get('dons'), $f3->get('dons_recurrents'));
        usort($history, function ($a, $b) {
            return strtotime($a['datec']) - strtotime($b['datec']);
        });
        $f3->set('historique', $history);
    }

    public function login($f3, $args)
    {
        $f3->set('form_visible', 'login');

        if ($f3->get('action')=='renew_password') {
            // Renouvellement de mot de passe
            $f3->clear('form_visible');
            $this->renew_password($f3);
        } elseif ($f3->get('action')=='create_account') {
            // Création d'un compte utilisateur
            $f3->clear('form_visible');
            $this->create_user($f3);
        } elseif ($f3->get('email')) {
            // Tentative de connexion
            // We should use auth module from Fat Free
            $db = $f3->get('DB');
            $mapper = new \DB\SQL\Mapper($f3->get('DB'), 'users');
            $auth = new \Auth($mapper, array('id' => 'email', 'pw' => 'hash'));
            $email = \Utils::asl($f3->get('email'));
            $hash = hash('sha256', $f3->get('password'));
            $login = $auth->login($email, $hash);
            
            // As we're login with email, not for id, we need to get it
            if ($login) {
                // We want to retrieve the associated user
                $result = $db->query("SELECT id, email FROM users WHERE email='".$email."'");
                // You can only fetch ONCE, unfortunately
                $res = $result->fetch(\PDO::FETCH_ASSOC);
                $id = $res['id'];
                $email = $res['email'];

                $f3->set('SESSION.id', $id);
                $f3->set('SESSION.email', $email);
                $f3->set('SESSION.user', true);
                $f3->clear('form_visible');
                $f3->set('block_content', 'user/perso.html');
            } else {
                $f3->push('SESSION.error', _("Mauvais identifiant ou mot de passe."));
            }
        } else {
            if ($f3->get('VERB') == 'POST') {
                $f3->push('SESSION.error', _("Merci de renseigner une adresse mail."));
            }
        }
        Campaign::show($f3, $args);
    }

    public function logout($f3, $args)
    {
        $f3->clear('SESSION');
        $f3->reroute('/');
    }

    public function renew_password($f3)
    {
        if ($f3->get('email')!='') {
            // On vérifie que l'adresse mail existe
            $db = $f3->get('DB');
            $result = $db->query("SELECT email FROM users WHERE email='".\Utils::asl($f3->get('email'))."';");
            $row = $result->fetch(\PDO::FETCH_ASSOC);
            if ($row['email']!='') {
                // On régénère un mot de passe
                $pass = \Utils::generate_password();
                // On ne conserve que le mot de passe haché
                $hash = hash('sha256', $pass);
                // On le sauve en base de données
                $query = "UPDATE users SET hash='".$hash."' WHERE email='".\Utils::asl($f3->get('email'))."';";
                $db->query($query);

                // On le renvoie par email
                // On utilise le système SMTP de fat free
                $mailer = new \SMTP(SMTP_HOST, SMTP_PORT, SMTP_SECURITY, SMTP_USER, SMTP_PW);
                $mailer->set('From', $f3->get('mail.from'));
                $mailer->set('To', $f3->get('email'));
                $mailer->set('Subject', _("Renouvellement de mot de passe"));

                $mailer->set('FromName', $f3->get('mail.fromName'));
                $mailer->set('CharSet', "UTF-8");

                // Préparation du texte du mail par morceaux
                $text = _("Bonjour,

                    Un nouveau mot de passe vient de vous être attribuer pour pouvoir vous connecter à votre page d'administration pour choisir vos contreparties, et nous indiquer vos coordonnées :
                    %%URL_ADMIN%%
                    Identifiant : votre adresse email
                    Mot de passe : %%PASSWORD%%

                    Encore merci pour votre soutien,
                    Toute l'équipe de La Quadrature du Net
                    ")."\n\n";
                // Création de l'url d'administration
                $admin_url = "https://don.laquadrature.net/perso";
                // Remplacement des variables par leurs valeurs
                $fields = array(
                    "PASSWORD" => $pass,
                    "URL_ADMIN" => $admin_url
                );
                foreach ($fields as $k=>$v) {
                    $text = str_replace("%%".$k."%%", $v, $text);
                }
                if ($mailer->send($text)) {
                    $f3->push('SESSION.message', _("Nouveau mot de passe envoyé par mail."));
                } else {
                    $logger = new \Log('mail.log');
                    $f3->push('SESSION.error', _("Oops."));
                    $logger->write($mailer->log());
                }
            } else {
                $f3->push('SESSION.error', _("Vous n'avez pas encore de compte, merci de faire un don pour cela."));
            }
        } else {
            $f3->push('SESSION.error', _("Merci de renseigner une adresse mail."));
        }
    }

      /**
     * create_user
     *
     * @param mixed $f3
     *
     * @return none
     */
    public function create_user($f3)
    {
        ///
        // Verify the information for the new account
        ///

        $new_account_email = $f3->get("email");
        $new_account_password = $f3->get("password");
        
        // error_log("New account email : ",3,"/home/don/don/log/error.log");
        // error_log(print_r($new_account_email, true) . "\n",3,"/home/don/don/log/error.log");
        // error_log("New account password : ",3,"/home/don/don/log/error.log");
        // error_log(print_r($new_account_password, true) . "\n",3,"/home/don/don/log/error.log");

        // Did the user give an email ?
        if (!$new_account_email || empty($new_account_email)){
            $f3->push('SESSION.error', _("Merci de renseigner une adresse email pour pouvoir créer un compte."));
            $f3->error('400');
        }

        // Did the user give a password ?
        if (!$new_account_password || empty($new_account_password)){
            $f3->push('SESSION.error', _("Merci de renseigner un mot de passe pour pouvoir créer un compte."));
            $f3->error('400');
        } 

        // Try to find an existing user account with an email matching the one given
        $does_an_account_exist = $f3->get('container')['user_finder']->findByEmail($new_account_email);
        // error_log("Does a user exists with the email address already ? : ",3,"/home/don/don/log/error.log");
        // error_log(print_r(!empty($does_an_account_exist), true) . "\n",3,"/home/don/don/log/error.log");

        if ( !empty($does_an_account_exist)) {
            $f3->push('SESSION.error', _("Désolé, un compte existe déjà avec cette adresse. Si vous avez perdu votre mot de passe, vous pouvez demander à le renouveller."));
            $f3->error('403');
        }

        ///
        // Creating the new user account
        ///
        
        $new_account_password_hash =  hash('sha256', $new_account_password);
        // error_log("Password hash for new user : ",3,"/home/don/don/log/error.log");
        // error_log(print_r($new_account_password_hash, true) . "\n",3,"/home/don/don/log/error.log");

        // Cumulate all the donnations originating from this email address to update the new account's balance
        try {
            $db = $f3->get('DB');
            $res = $db->query("SELECT sum(dons.somme) as somme FROM dons INNER JOIN users WHERE dons.user_id = users.id AND users.email='".\Utils::asl($f3->get('email'))."' and dons.status in (1, 4, 102) and dons.datec>'2013-01-01';");
            $total = $res->fetch(\PDO::FETCH_ASSOC);
        } catch (\Exception $th) {
            // error_log("Error while getting balance : ",3,"/home/don/don/log/error.log");
            // error_log(print_r($th->getMessage(), true) . "\n",3,"/home/don/don/log/error.log");    
            $f3->push('SESSION.error', _("Une erreur est survenue lors de la création de votre compte, merci de réessayer."));            
            $f3->error('500');
        }
        
        $total = (int) $total['somme'];
        // error_log("New account existing total balance : ",3,"/home/don/don/log/error.log");
        // error_log(print_r($total, true) . "\n",3,"/home/don/don/log/error.log");

        // Inject the new user to the database 
        try {
            $f3->get('container')['command_handler']->handle(new UserCreateCommand($new_account_email, $new_account_password_hash, '', $total, $total));
        } catch (\Exception $th) {
            // error_log("Error while injecting the user ",3,"/home/don/don/log/error.log");
            // error_log(print_r($th, true) . "\n",3,"/home/don/don/log/error.log");
            $f3->push('SESSION.error', _("Une erreur est survenue lors de la création de votre compte,".$th->getMessage()." merci de réessayer."));            
            $f3->error('500');
        }

        $f3->push('SESSION.message', _("Votre compte a bien été créé, merci."));
        // error_log("New user created \n",3,"/home/don/don/log/error.log");
        $f3->reroute('/perso');
    }

    public function infos($f3, $args)
    {
        $db = $f3->get('DB');

        if ($f3->exists('SESSION.id')) {
            // Let's firts check that both the password match
            if ($f3->get('password') != $f3->get('password_confirmation')) {
                $f3->set('SESSION.error', _("Les mots de passe ne correspondent pas"));
            } else {
                $sql = "UPDATE users SET email='".\Utils::asl($f3->get('email'))."',
                    pseudo='".\Utils::asl($f3->get('pseudo'))."'";
                if ($f3->get('password') != '') {
                    $hash = hash('sha256', $f3->get('password'));
                    $sql .= ", hash='".$hash."'";
                }
                $sql .= " WHERE id='".\Utils::asl($f3->get('SESSION.id'))."'";
                $db->query($sql);
            }
        } else {
            $f3->error(401);
        }
        $f3->set('SESSION.msg', _('Informations personnelles modifiées.'));
        $this->show($f3, $args);
    }

    public function adresses($f3, $args)
    {
        if (!$f3->exists('SESSION.id')) {
            $f3->error('401');
        }

        switch ($f3->get('action')) {
        case 'ADD':
            try {
                $f3->get('container')['command_handler']->handle(
                    new AddressCreateCommand(
                        \Utils::asl($f3->get('SESSION.id')),
                        \Utils::asl($f3->get('nom')),
                        \Utils::asl($f3->get('adresse')),
                        \Utils::asl($f3->get('adresse2')),
                        \Utils::asl($f3->get('codepostal')),
                        \Utils::asl($f3->get('ville')),
                        \Utils::asl($f3->get('pays')),
                        \Utils::asl($f3->get('state'))
                    )
                );
            } catch (AddressAlreadyExistsException $e) {
                $f3->set("error", _("Cette adresse existe déjà."));
                $f3->error('403');
            }
            $f3->push('SESSION.message', _("Adresse ajoutée à votre profil"));
            break;
        case 'UPDATE':
            $f3->get('container')['command_handler']->handle(
                new AddressUpdateCommand(
                    \Utils::asl($f3->get('id')),
                    \Utils::asl($f3->get('SESSION.id')),
                    \Utils::asl($f3->get('nom')),
                    \Utils::asl($f3->get('adresse')),
                    \Utils::asl($f3->get('adresse2')),
                    \Utils::asl($f3->get('codepostal')),
                    \Utils::asl($f3->get('ville')),
                    \Utils::asl($f3->get('pays')),
                    \Utils::asl($f3->get('state'))
                )
            );
            $f3->push('SESSION.message', _("Adresse du profil modifiée."));
            break;
        case 'DELETE':
            try {
                $f3->get('container')['command_handler']->handle(
                    new AddressDeleteCommand(
                        \Utils::asl($f3->get('id')),
                        \Utils::asl($f3->get('SESSION.id'))
                    )
                );
            } catch (AddressUsedException $e) {
                $f3->set("error", _("Cette adresse est utilisée pour une de vos contreparties"));
                $f3->error('403');
            };
            $f3->push('SESSION.message', _("Adresse supprimée de votre profil"));
            break;
        }
        $f3->reroute('/perso');
    }

    public function add_contrepartie($f3, $args)
    {
        if (!$f3->exists('SESSION.user')) {
            $f3->error('401');
        }

        $db = $f3->get('DB');
        $user = $f3->get('container')['user_finder']->findById($f3->get('SESSION.id'));

        $adresse_id = $f3->get('container')['address_finder']->findByUserId($f3->get('SESSION.id'))["id"];
        $quoi = $f3->get('quoi');
        $commentaire = $f3->get('commentaire');
        $valeur = 0;
        $taille = intval($f3->get('taille'));
        $taille_h = intval($f3->get('taille_h'));

        // Vérification des données avant l'ajout des contreparties 
        if (empty($adresse_id) || empty($quoi)) {
            $f3->push('SESSION.error', _("Désolé, votre demande à été mal formulée, veulliez réessayer ou nous envoyer un email."));
            $f3->error('400');
        }

        switch ($quoi) {
            case 'pibag':
                $valeur = 64;
                break;
            case 'pishirt':
                $valeur = 128;
                break;
            case 'hoodie':
                $valeur = 314;
                break;
            default:
                $f3->push('SESSION.error', _("Désolé, mais vous avez demandé une contrepartie impossible..."));
                $f3->error('400');
                break;
        }

        // Ajout d'une demande de contrepartie pour chaque contrepartie
        if ((int) $user['cumul'] >= $valeur) {
            
            $parent = $f3->get('container')['counterpart_finder']->getNextInsertedId();
            
            switch ($quoi) {
                case 'hoodie':
                    $f3->get('container')['command_handler']->handle(new CounterpartCreateCommand(\Utils::asl($adresse_id), $f3->get('SESSION.id'), 'hoodie', \Utils::asl($taille_h), 1, date("Y-m-d H:i:s"), \Utils::asl($commentaire), $parent));
                    // no break
                case 'pishirt':
                    $f3->get('container')['command_handler']->handle(new CounterpartCreateCommand(\Utils::asl($adresse_id), $f3->get('SESSION.id'), 'pishirt', \Utils::asl($taille), 1, date("Y-m-d H:i:s"), \Utils::asl($commentaire), $parent));
                    // no break
                case 'pibag':
                    $f3->get('container')['command_handler']->handle(new CounterpartCreateCommand(\Utils::asl($adresse_id), $f3->get('SESSION.id'), 'pibag', 1, 1, date("Y-m-d H:i:s"), \Utils::asl($commentaire), $parent));
                    break;
            }

            // Puis diminution du cumul de la valeur des contreparties
            $f3->get('container')['command_handler']->handle(new UserUpdateCumulCommand($user['id'], $user['cumul'] - $valeur));
            $f3->push('SESSION.message', _("Merci, vos contreparties seront envoyées dès que possible !".$valeur));
        
        } else {
            $f3->push('SESSION.error', _("Désolé, mais vous n'avez pas cumulé suffisament de dons pour avoir un ").$quoi);
            $f3->clear('SESSION.message');
        }
        $f3->reroute('/perso');
    }

    public function receipt($f3, $params)
    {
        if (!$f3->exists('SESSION.user')) {
            $f3->reroute('/login');
        }
        // Variables utiles
        $logger = new \Log('receipt.log');
        $db = $f3->get('DB');
        $id = $params['id'];
        $command = "pdftk ";
        $args = " ../www/receipt.pdf fill_form ../tmp/".escapeshellarg($id).".xfdf output ../tmp/".escapeshellarg($id).".pdf flatten dont_ask";
        $logger->write($command . " " .$args);

        // Vérification que le don appartient bien à l'utilisateur connecté
        $query="SELECT dons.datec,
            dons.somme,
            adresses.nom as pseudo,
            adresses.adresse,
            adresses.adresse2,
            adresses.codepostal,
            adresses.ville,
            adresses.pays
            FROM dons
            INNER JOIN users ON users.id = dons.user_id
            LEFT OUTER JOIN adresses ON adresses.user_id = users.id
            WHERE users.id='".\Utils::asl($f3->get('SESSION.id'))."' AND dons.id='".\Utils::asl($id)."';";
        $result = $db->query($query);
        $logger->write($query);
        $don = $result->fetch(\PDO::FETCH_ASSOC);
        if (!$don) {
            $f3->reroute("/perso");
        }

        // Création du fichier datas
        $name = $don['pseudo'];
        $address = $name;
        if ($don['adresse']!='') {
            $address .= "\n" . $don['adresse'];
            $address .= "\n" . $don['adresse2'];
            $address .= "\n" . $don['codepostal'] . " " . $don['ville'];
        }
        $amount = $don['somme'];
        $monthes = array(
            '01'=>_('Janvier'),
            '02'=>_('Février'),
            '03'=>_('Mars'),
            '04'=>_('Avril'),
            '05'=>_('Mai'),
            '06'=>_('Juin'),
            '07'=>_('Juillet'),
            '08'=>_('Août'),
            '09'=>_('Septembre'),
            '10'=>_('Octobre'),
            '11'=>_('Novembre'),
            '12'=>_('Décembre')
        );
        $date = substr($don['datec'], 8, 2)." ".$monthes[substr($don['datec'], 5, 2)]." ".substr($don['datec'], 0, 4);
        $content = '<?xml version="1.0" encoding="UTF-8"?>
            <xfdf xmlns="http://ns.adobe.com/xfdf/" xml:space="preserve">
            <fields>
            <field name="address">
            <value>'.$address.'</value>
            </field>
            <field name="donor">
            <value>'.$name.'</value>
            </field>
            <field name="content">
            <value>Reçu le '.$date.' un don de '.$amount.' euros de la part de '.$name.'.</value>
            </field>
            <field name="date">
            <value>Fait à Paris le '.$date.'</value>
            </field>
            </fields>
            </xfdf>';
        $fp = fopen("../tmp/".$id.".xfdf", "w");
        if ($fp) {
            fputs($fp, $content);
            fclose($fp);
        }

        // Création du reçu
        $output = array();
        exec($command.$args, $output);

        // Suppression du fichier datas
        unlink("../tmp/".$id.".xfdf");

        // Renvoi du fichier pdf au client
        $fp = fopen("../tmp/".$id.".pdf", "rb");
        if ($fp) {
            $fsize = filesize("../tmp/".$id.".pdf");
            header("Content-Type: application/pdf");
            header("Content-Disposition: attachment; filename=\"".$id.".pdf\"");
            header("Content-Length: $fsize");
            while (!feof($fp)) {
                $buffer = fread($fp, 2048);
                echo $buffer;
            }
            fclose($fp);
        } else {
            $f3->reroute('/perso');
        }

        // Suppression du fichier reçu
        unlink("../tmp/".$id.".pdf");
        die();
    }

    public function cancel($f3, $params)
    {
        if (!$f3->exists('SESSION.user')) {
            $f3->reroute('/login');
        }
        $db = $f3->get('DB');
        $result = $db->query("SELECT identifier, user_id from identifiers where identifier like '".\Utils::asl($params['id'])."' and user_id='".\Utils::asl($f3->get('SESSION.id'))."'");
        $result = $result->fetch(\PDO::FETCH_ASSOC);
        if ($result) {
            $identifier = $result['identifier'];
            $user_id = $result['user_id'];
        } else {
            $f3->reroute('/perso');
        }
        // Commande customerCancel
        $date = new \DateTime('now', new \DateTimeZone("UTC"));
        $parameters = array(
            "shopId"    => SITE_ID,
            "cardIdent" => $identifier,
            "date"      => $date->format("Ymd"),
            "ctxMode"   => CTX_MODE,
        );
        // Calcul de la signature
        $signature = "";
        foreach ($params as $key=>$value) {
            $signature .= $value."+";
        }
        $signature .= CERTIFICATE;
        $signature = base64_encode(hash_hmac('sha256', $signature, CERTIFICATE, true));
        #$signature = sha1($signature);
        $parameters["wsSignature"] = $signature;
        $client = new \SoapClient("https://paiement.systempay.fr/vads-ws/ident-v2.1?wsdl");
        $result = $client->customerCancel($parameters);
        // Let's add a 103 don at 0€ to keep track of them
        $f3->query("INSERT INTO dons SET
            status = '103',
            datec = NOW(),
            somme = 0,
            user_id = '".\Utils::asl($user_id)."',
            identifier = '".\Utils::asl($identifier)."',
            cumul = 0;");
        $f3->push('SESSION.message', _("Don récurrent supprimé."));
        $f3->reroute('/perso');
    }

    public function renew($f3, $params)
    {
        // TODO: à écrire, finaliser, tester, mettre en prod :o)
/*
    // Variables utiles
    $id = $params['id'];

    // Cas S2 de mise à jour des infos de la CB
    $target = "https://paiement.systempay.fr/vads-payment/";
    $transaction_date = new \DateTime('now', new \DateTimeZone("UTC"));
    $params = array(
        // Champs obligatoires
        "vads_trans_date" => $transaction_date->format("YmdHis"),
        "vads_site_id" => SITE_ID,
        "vads_action_mode" => "INTERACTIVE",
        "vads_ctx_mode" => CTX_MODE,
        // Autres codes possibles (page 16)
        "vads_trans_id" => str_repeat("0",6-strlen($id)).$id,
        "vads_version" => "V2",
        // Champs facultatifs
        "vads_language" => $f3->get('lang'),
        "vads_order_id" => $id,
        "vads_url_cancel" => ROOTURL,
        "vads_url_check" => "",
        "vads_url_error" => ROOTURL,
        "vads_url_referral" => ROOTURL,
        "vads_url_refused" => ROOTURL,
        "vads_url_return" => ROOTURL,
        "vads_url_success" => ROOTURL,
        "vads_validation_mode" => "0",
        "vads_shop_name" => "La Quadrature du Net",
        "vads_shop_url" => ROOTURL
    );
    if ($f3->get('monthly')) {
        // En cas de paiement récurrent, on doit créer un compte carte si ce n'est pas déjà fait
        $result = mysql_fetch_array(mysql_query("SELECT identifier from users where email='".$email."'"));
        $identifier = "";
        if ($result) {
        $identifier = $result['identifier'];
        }
        if ($identifier=="") {
        // Si l'utilisateur n'existe pas déjà, on lui crée un identifiant
        $identifier = $id . "_" . substr($email,0,strpos($email,'@'));
        $params["vads_identifier"] = substr($identifier, 0, 50); // Pas plus de 50 caractères
        $params["vads_page_action"] = "REGISTER_SUBSCRIBE";
        $params["vads_cust_email"] = $email; // Email du porteur
        } else {
        // Sinon, on instaure juste une récurrence en utilisant l'identifiant déjà connu
        $params["vads_page_action"] = "SUBSCRIBE";
        $params["vads_identifier"] = $identifier;
        }
        $params["vads_sub_effect_date"] = date("Ymd"); // Date d'effet à ce jour
        $params["vads_sub_amount"] = $f3->get('sum')*100;
        $params["vads_sub_currency"] = "978";
        $params["vads_sub_desc"] = "RRULE:FREQ=MONTHLY;BYMONTHDAY=7"; // Tous les 7 du mois
    } else {
        // En cas de paiement ponctuel, le montant est donné différemment
        $params["vads_page_action"] = "PAYMENT";
        $params["vads_amount"] = $f3->get('sum')*100;
        $params["vads_currency"] = "978";
        $params["vads_payment_config"] = "SINGLE";
    }
    // Calcul de la signature
    ksort($params);
    $signature = "";
    foreach ($params as $key=>$value) {
        $signature .= $value."+";
    }
    $signature .= CERTIFICATE;
    $signature = sha1($signature);

    $f3->set('target', $target);
    $f3->set('params', $params);
    $f3->set('signature', $signature);
 */
    }
};
