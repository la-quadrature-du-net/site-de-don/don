<?php
// /*
//  * This class is used to manage cronjobs.
//  * We use the PHP_SAPI to be sure to refuse to run on a webserver.
//  * The command should be called like this :
//  *     php index.php "/action"
//  */
// namespace Controller;

// class Cron extends Controller
// {
//     public function beforeRoute($f3, $args)
//     {
//         parent::beforeRoute($f3, $args);
//     }

//     public function afterRoute($f3, $args)
//     {
//     }
// }