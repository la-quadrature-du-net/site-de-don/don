<?php
namespace Controller;

class Controller
{
    // Constructeur
    public function __construct()
    {
        $f3=\Base::instance();
    }

    // Fonction appelée avant routage
    public function beforeRoute($f3, $args)
    {
        // Template de base par défaut
        $this->template = "base.html";

        if (php_sapi_name() == 'cli') {
            $HTTP_HOST = 'localhost';
        } else {
            $HTTP_HOST = $_SERVER['HTTP_HOST'];
        }
        define("ROOTURL", "https://".$HTTP_HOST.'/');

        // Sélection de la langue par header
        $lang = 'fr_FR';
        $lang_short = 'fr';

        if ($f3->exists('GET.lang')) {
            // On force la langue passée dans l'URL GET
            $accept_languages = [$f3->get('GET.lang')];
            $f3->set('SESSION.lang', $f3->get('GET.lang'));
        } else {
            if ($f3->exists('SESSION.lang')) {
                // On a déjà défini une langue précédemment
                $accept_languages = [$f3->get('SESSION.lang')];
            } else {
                // On récupère la langue du navigateur
                $accept_languages = $f3->exists('HEADERS.Accept-Language') ? explode(',', $f3->get('HEADERS.Accept-Language')) : [];
            }
        }

        foreach (explode(',', LANGUAGES) as $language) {
            foreach ($accept_languages as $accept_language) {
                if (substr($language, 0, strlen($accept_language))===$accept_language) {
                    $lang = $language;
                    $lang_short = explode($language, '-')[0];
                }
            }
        }

        if (!bindtextdomain("messages", "../locales/")) {
            echo "<!-- bondtextdomain failed -->";
        }
        // Language ok, set the locale environment
        putenv("LC_MESSAGES=".$lang);
        putenv("LANG=".$lang);
        putenv("LANGUAGE=".$lang);

        if (!setlocale(LC_ALL, $lang)) {
            // try the UTF8 version
            putenv("LC_MESSAGES=".$lang.".utf8");
            putenv("LANG=".$lang.".utf8");
            putenv("LANGUAGE=".$lang.".utf8");
            if (!setlocale(LC_ALL, $lang.".utf8")) {
                echo "<!-- setlocale $lang failed -->";
            }
        }

        if (!textdomain("messages")) {
            echo "<!-- textdomain failed -->";
        }
        $charset = "UTF-8";
        bind_textdomain_codeset("messages", $charset);

        // Variables pour les templates
        $f3->set('lang', $lang);
        $f3->set('lang_short', $lang_short);

        // Initialize DB
        $f3->set('DB', new \DB\SQL(
            SQL_DSN,
            SQL_USER,
            SQL_PASSWORD,
            array( \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION)
        ));

        if ($f3->get('VERB') == 'POST') {
            $this->sanitizeForms($f3);
        }

        $f3->set('mail', array(
            "from" => "contact@laquadrature.net",
            "fromName" => "La Quadrature du Net"
        ));
        $atailles = array(
            1 => _('Coupe Homme, Taille') . ' S',
            2 => _('Coupe Homme, Taille') . ' M',
            3 => _('Coupe Homme, Taille') . ' L',
            4 => _('Coupe Homme, Taille') . ' XL',
            5 => _('Coupe Femme, Taille') . ' S',
            6 => _('Coupe Femme, Taille') . ' M',
            7 => _('Coupe Femme, Taille') . ' L',
            8 => _('Coupe Femme, Taille') . ' XL',
            9 => _('Coupe Homme, Taille') . ' XXL',
            10 => _('Coupe Femme, Taille') . ' XXL',
        );
        asort($atailles);
        $f3->set('atailles', $atailles);

        // On initialise les messages en tableau vide, par défaut
        $f3->set('SESSION.message', array());
        $f3->set('SESSION.error', array());

        // Ceci n'est valable que pour les tests en CLI
        if (strlen($f3["RESPONSE"])>0) {
            $f3->clear("RESPONSE");
            return;
        }
    }

    // Fonction appelée après routage
    public function afterRoute($f3, $args)
    {
        // Ceci n'est valable que pour les tests en CLI
        if (strlen($f3["RESPONSE"])>0) {
            echo $f3["RESPONSE"];
            return;
        }

        // Test si il y a des messages
        if (!$f3->exists('SESSION.message')) {
            $f3->push('SESSION.message', '');
        }
        if (!$f3->exists('SESSION.error')) {
            $f3->push('SESSION.error', '');
        }
        // Rendu de la page
        if ($this->template!='') {
            echo \Template::instance()->render($this->template);
        }
        // Une fois que tout est affiché, on peut supprimer les notifications
        $f3->clear('SESSION.message');
        $f3->clear('SESSION.error');
    }

    // Fonction utilisée pour sanitiser les données
    public function sanitizeForms($f3)
    {
        // We need to check we do have a POST
        if ($f3->get('VERB') != 'POST') {
            return;
        }

        // CSRF checks
        //if ($f3->get('POST.csrf') != $f3->get('container')['session']->get('csrf')) {
        //   $f3->error('400', 'CSRF Validation error');
        //}

        // First, let's clean all the data
        // actual data validation is done in the functions
        foreach ($f3->get('POST') as $key => $value) {
            $f3->set($key, $f3->clean($value));
        }
    }
};
