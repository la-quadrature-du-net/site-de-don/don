<?php

if (!file_exists(__DIR__ . '/env')) {
    throw new \RuntimeException('No "env" file found.');
}

$dotenv = new Dotenv\Dotenv(__DIR__, 'env');
$dotenv->load();

/* Configuration file for Campaign manager */
define("SYSADMIN", getenv('SYSADMIN'));
define("FDNNURL1", getenv('FDNNURL1'));
define("FDNNURL2", getenv('FDNNURL2'));
define("BASE_DOMAIN", getenv('BASE_DOMAIN'));

define("SQL_HOST", getenv('SQL_HOST'));
define("SQL_PORT", getenv('SQL_PORT'));
define("SQL_DATABASE", getenv('SQL_DATABASE'));
define("SQL_DSN", "mysql:host=".SQL_HOST.";port=".SQL_PORT.";dbname=".SQL_DATABASE.";charset=utf8");
define("SQL_USER", getenv('SQL_USER'));
define("SQL_PASSWORD", getenv('SQL_PASSWORD'));

define("PAYMENT_URL", getenv('PAYMENT_URL'));
define("RETURN_CHECK_URL", getenv('RETURN_CHECK_URL'));
define("SITE_ID", getenv('SITE_ID'));
define("CERTIFICATE", getenv('CERTIFICATE'));
define("CTX_MODE", getenv('CTX_MODE'));
//define("CERTIFICATE","XXXXXXXXXXXXXXXXXXXXXXXXx");
//define("CTX_MODE","PRODUCTION");

define("LANGUAGES", "fr_FR,en_US");

define("PIPLOME_PATH", getenv('PIPLOME_PATH'));
define("PIPLOME_URL", getenv('PIPLOME_URL'));

define("CAMPAIGN_START_DATE", getenv('CAMPAIGN_START_DATE'));
define("CAMPAIGN_BUDGET", getenv('CAMPAIGN_BUDGET'));

//Logs
define("LOGS", getenv('LOGS'));

// SMTP connexions
define("SMTP_HOST", getenv('SMTP_HOST'));
define("SMTP_PORT", getenv('SMTP_PORT'));
define("SMTP_SECURITY", getenv('SMTP_SECURITY')); //tls, ssl, or none
define("SMTP_USER", getenv('SMTP_USER'));
define("SMTP_PW", getenv('SMTP_PW'));

define("DEBUG", getenv('DEBUG'));

define("SENTRY", getenv('SENTRY'));

// $sentry_client = new Raven_Client($SENTRY);
// $error_handler = new Raven_ErrorHandler($sentry_client);
// $error_handler->registerExceptionHandler();
// $error_handler->registerErrorHandler();
// $error_handler->registerShutdownFunction();

if ('prod' !== $env) {
    error_reporting(E_ALL|E_WARNING);
    ini_set("display_errors", true);
}
