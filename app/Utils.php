<?php

class Utils
{

/*
Cette fonction définit un mot de passe de 12 caractères
 */
    public static function generate_password()
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i=0;$i<12;$i++) {
            $n = rand(0, $alphaLength);
            $pass .= $alphabet[$n];
        }
        return $pass; //turn the array into a string
    }


    /*
    Cette fonction cherche à nettoyer un mauvais encodage en utf-8.
     */
    public static function clean_encoding($string)
    {
        $bad_encoding = array("Ã©","Ã ","Ã¨","Ã§","Ã«","Ã´","Ã®","Ã¹");
        $good_encoding = array("é","à","è","ç","ë","ô","î","ù");

        $string = str_replace($bad_encoding, $good_encoding, $string);
        return $string;
    }

    /* AddSlashes contextuel : ne fait addslashes que si magic-quotes est OFF */
    public static function asl($str)
    {
        return addslashes($str);
    }

};
