# ![](www/static/img/cochon-01.png) Site de Don de La Quadrature Du Net

Ce site web, écrit en PHP avec le framework Fat-Free-Framework pour nous permettre de gérer les donnations et les contreparties pour La Quadrature Du Net. ( English below )

## Conception

Ce site à été conçu de zéro, par des bénévoles, des salariés et des membres de La Quadrature Du Net.

## Installation

Vous aurez besoin de ;

- Un serveur Linux ( Debian de préférence ), avec...
    - PHP 7.4+
    - Mysql/MariaDB

Cela n'est pas nécessaire pour le développement du site.

### En production

Afin de faciliter le déployement, vous pouvez utiliser une recette Ansible pour le déployement en production. [Vous pouvez la consulter ici](https://git.laquadrature.net/lqdn-interne/piops-roles/don-lqdn).

Autrement, vous aurez besoin de :

##### Téléchargement

Vous pouvez obtenir les dernières version du site de don sur [la page des publication.](https://git.laquadrature.net/lqdn-interne/don/-/releases)

##### Configuration du serveur

L'installation des dépendances se fait via [Composer](https://getcomposer.org/).

##### Configuration des paramètres du site web

Vous aurez besoin de remplir les variables du fichier `.env` dans le dossier `app/`. Dupliquez le fichier `env.sample`, renommez le en `.env` et remplissez le.

##### Serveur web

Vous pouvez vous inspirer de la configuration décrite dans `ansible/configuration.test.yml` pour votre serveur Nginx.

##### Connexion à la banque

> À completer

##### Gestion

Il existe des comptes de test par défaut, n'oubliez pas de les supprimer avant la mise en production !

- `alice@example.org` : `password`

- `bob@example.org` : `password`

Il existe aussi un compte administrateur de test. Vous pouvez accéder à l'interface d'administration via le chemin `https://<url du site>/admin`.

* `admin` : `password`

### En test

Afin de faciliter les test, nous avons inclus un fichier Vagrant, qui permet de lancer une machine virtuelle pré-configurée avec toutes les dépendances nécessaire pour tester le site de don, à l'exception du mode de banque pour le moment ( mais rien n'empêche que vous le mettiez en place. )

Afin de lancer la machine de développement, vous pouvez ;

- Installer Vagrant si ce n'est pas déjà fait,
- Lancer `vagrant up`. Ça prendra un peu de temps la première fois.
- Vous avez maintenant une machine virtuelle accessible via ssh avec `vagrant ssh`, et vous pouvez voir le site de don sur `localhost:8282`.
- Vous pouvez faire vos modifications directement dans le dossier du dépôt, car il est lié sur la machine virtuelle, sous le chemin `/vagrant`.

Le déployement se fait dans la machine virtuelle de test, au moyen de la recette ansible [don-lqdn](https://git.laquadrature.net/lqdn-interne/piops-roles/don-lqdn), que vous pouvez lire directement dans le dossier `ansible/`

## Développement

Quand vous lancez les tests avec Vagrant, vous avez accès au site de don dans sa version définie par le playbook. Par défaut, c'est la version sur `master`.

Vous pouvez aussi travailer sur votre version locale en modifiant les variables du playbook dans `ansible/configuration.test.yml`. Modifiez la variable `root` de la configuration nginx de la façon suivante ;

```
root: /vagrant/www
```

### Documentation

Vous pouvez consulter la documentation relative au développement et au déployement de ce site web dans le dossier `doc/` ou en [cliquant ici.](doc/README.md)

## Contributions

Les contributions sont bienvenues ! Pour commencer, vous pouvez regarder les tickets ouvert sur le dépôt. Certaines sont marqués comme étant "besoin d'aide", et ne demandent que vous pour être résoluts !

Afin de faciliter le développement collaboratif, vous pouvez commencer par commenter le ticket qui vous intéresse, et expliquer ce que vous comptez faire.

Sinon, vous pouvez aussi ouvrir un ticket pour proposer des améliorations, des suggestions, ou autre.

Vous pouvez ensuite ouvrir une merge request, et une fois relue, elle sera mise en place ! À ce stade, merci beaucoup :)

## Licence

Ce site web est placé sur licence *GNU Affero General Public License v3.0.*.

----

[ 🇬🇧 Read the english version by clicking this link.](README.en.md)
