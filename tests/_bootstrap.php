<?php
// This is global bootstrap for autoloading

$config = __DIR__ . '/../app/config.php';
if (file_exists($config)) {
    require_once $config;
} elseif (!defined(SQL_HOST)) {
    throw new \Exception('No config found');
}
