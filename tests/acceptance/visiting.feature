Feature: visiting
  In order to visit the website
  As an anonymous user
  I need to see the Homepage

  Scenario: Visiting the home page
  Given I am on page "/"
  Then I should see "Soutenez La Quadrature du Net"
