<?php

/*
$I = new AcceptanceTester($scenario);
$I->wantTo('Ensure that I can deal with addresses.');

$I->login('alice@example.org', 'password');

# Addresses are here
$I->amOnPage('/perso');
$I->see('Main', '#section-addresses');
$I->see('Second address', '#section-addresses');
$I->see('Third address', '#section-addresses');

# Let's remove the last address
$I->submitForm('form#del4', []);
$I->amOnPage('/perso');
$I->see('Main', '#section-addresses');
$I->see('Second address', '#section-addresses');
$I->dontSee('Third address', '#section-addresses');

# Let's create a new one
$I->submitForm('form#create-address-form', [
    'alias' => 'Brand new address',
    'nom' => 'Alice',
    'adress' => '6 rue Ménars',
    'zipcode' => '22000',
    'ville' => 'Saint-Brieux',
    'state' => 'Côtes d\'Armor',
    'pays' => 'France',
    'default' => false,
]);
$I->amOnPage('/perso');
$I->dontSee('Third address', '#section-addresses');
$I->see('Brand new address', '#section-addresses');

# Let's update an address
$I->submitForm('form#up2', [
    'alias' => 'New address name',
    'adress' => '6 rue Ménars',
    'zipcode' => '75002',
    'ville' => 'Paris',
    'state' => 'Idf',
    'pays' => 'France',
    'default' => false,
]);
$I->amOnPage('/perso');
$I->dontSee('Second address', '#section-addresses');
$I->see('New address name', '#section-addresses');
*/
