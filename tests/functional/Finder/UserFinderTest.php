<?php

namespace LQDN\Tests\Functional\Finder;

use LQDN\Tests\Functional\FunctionalTest;

class UserFinderTest extends FunctionalTest
{
    public function testFindById()
    {
        $user = $this->container['user_finder']->findById(1);

        $expectedUser = [
            'id' => '1',
            'status' => '1',
            'hash' => '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',
            'cumul' => '2000',
            'email' => 'alice@example.org',
            'total' => '5000',
            'pseudo' => 'Alice',
            'expiration' => null,
            'commentaire' => 'RAS',
        ];
        $this->assertEquals($expectedUser, $user);
    }

    public function testFindByEmail()
    {
        $user = $this->container['user_finder']->findByEmail('alice@example.org');

        $expectedUser = [
            'id' => '1',
            'status' => '1',
            'hash' => '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',
            'cumul' => '2000',
            'email' => 'alice@example.org',
            'total' => '5000',
            'pseudo' => 'Alice',
            'expiration' => null,
            'commentaire' => 'RAS',
        ];
        $this->assertEquals($expectedUser, $user);
    }

    public function testFindByPseudo()
    {
        $user = $this->container['user_finder']->findByPseudo('Alice');

        $expectedUser = [
            'id' => '1',
            'status' => '1',
            'hash' => '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',
            'cumul' => '2000',
            'email' => 'alice@example.org',
            'total' => '5000',
            'pseudo' => 'Alice',
            'expiration' => null,
            'commentaire' => 'RAS',
        ];
        $this->assertEquals($expectedUser, $user);
    }


    public function testAdminSearch()
    {
        $this->assertCount(1, $this->container['user_finder']->adminSearch($text='alice@example.org', $status=''));
        $this->assertCount(2, $this->container['user_finder']->adminSearch($text='', $status='1'));

    }
}
