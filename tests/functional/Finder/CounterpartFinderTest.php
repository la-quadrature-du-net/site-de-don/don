<?php

namespace LQDN\Tests\Functional\Finder;

use LQDN\Tests\Functional\FunctionalTest;

class CounterpartFinderTest extends FunctionalTest
{
    public function testFindByUserId()
    {
        $counterparts = $this->container['counterpart_finder']->findByUserId(2);
        $this->assertCount(3, $counterparts);

        $firstCounterpart = reset($counterparts);

        // Check the first counterpart
        $expectedCounterpart = [
            'id' => '2',
            'user_id' => '2',
            'datec' => '2016-06-22 12:34:00',
            'quoi' => 'hoodie',
            'taille' => '2',
            'status' => '2',
            'adresse_id' => null,
            'pdf_id' => '1',
            'pdf_nom' => 'Main',
            'pdf_url' => 'pdf',
            'commentaire' => '',
            'parent' => '4',
        ];
        $this->assertEquals($expectedCounterpart, $firstCounterpart);
    }

    public function testFindByQuoi()
    {
        $counterparts = $this->container['counterpart_finder']->findByQuoi('hoodie');
        $this->assertCount(1, $counterparts);
        $firstCounterpart = reset($counterparts);

        // Check the first counterpart
        $expectedCounterpart = [
            'id' => '2',
            'user_id' => '2',
            'datec' => '2016-06-22 12:34:00',
            'quoi' => 'hoodie',
            'taille' => '2',
            'status' => '2',
            'adresse_id' => null,
            'pdf_id' => '1',
            'pdf_nom' => 'Main',
            'pdf_url' => 'pdf',
            'commentaire' => '',
            'parent' => '4',
        ];
        $this->assertEquals($expectedCounterpart, $firstCounterpart);

    }

    public function testFindById()
    {
        $counterparts = $this->container['counterpart_finder']->findByStatus(1);
        $this->assertCount(1, $counterparts);

        $firstCounterpart = reset($counterparts);
        $expectedCounterpart = [
            'id' => '1',
            'datec' => '2016-06-22 12:34:00',
            'user_id' => '1',
            'quoi' => 'pishirt', // [piplome|pibag|pishirt|hoodie]
            'taille' => '8',
            'status' => '1',
            'adresse_id' => '1',
            'pdf_id' => '',
            'pdf_nom' => '',
            'pdf_url' => '',
            'commentaire' => '',
            'parent' => '1',
        ];
        $this->assertEquals($expectedCounterpart, $firstCounterpart);
    }
}
