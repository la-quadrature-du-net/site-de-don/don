<?php

require_once("../vendor/autoload.php");

require_once("../app/config.php");

// Retrieve instance of the framework
$f3 = Base::instance();

// Initialize CMS
$f3->config('../app/config.ini');

$f3->set('container', new LQDN\Container());

// Deal with sessions
$session = $f3->get('container')['session'];
$session->start();
if (!$session->has('csrf')) {
    $session->set('csrf', md5(rand()));
}
$f3->CSRF = $session->get('csrf');

// Initialisation éventuelle des messages
if (!$session->has('message')) {
    $session->set('message', []);
}

if (!$session->has('error')) {
    $session->set('error', []);
}

// Let's register the message tag
\Template::instance()->extend('message', 'MessageHelper::render');

// Debug
$f3->set('DEBUG', DEBUG);
$f3->set('ONERROR', function ($f3) {
    $logger = new Log('error.log');
    $logger->write($f3->get('ERROR.code') . ' - ' . $f3->get('ERROR.text'));
    $logger->write($f3->get('ERROR.trace'));

    // If there is any problem, replace the current csrf
    $csrf = md5(rand());
    $f3->CSRF = $csrf;

    // If neither error or message are set, define them
    if (!$f3->exists('SESSION.message')) {
        $f3->push('SESSION.message', []);
    }
    if (!$f3->exists('SESSION.error')) {
        $f3->push('SESSION.error', []);
    }
    $f3->get('container')['session']->set('csrf', $csrf);

    while (ob_get_level()) {
        ob_end_clean();
    }
    $f3->set('block_content', 'campaign/errors.html');
    echo \Template::instance()->render('base.html');
});

// Are we being invoked from CLI ?
if (php_sapi_name() == 'cli') {
    $f3->CLI = true;
}

// Define routes
$f3->config('../app/routes.ini');

// Execute application
$f3->run();
