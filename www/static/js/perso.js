/*
Javascript utilisé par le site de campagne, la page d'administration personnelle.
*/

/*
Gestion des contreparties sur la page d'administration
*/
$("#a2 .cntrprt").click(function(e){
    $('#a2 #receive_main').show();
    $('#a2 #receive_pibag').hide();
    $('#a2 #receive_pishirt').hide();
    $('#a2 #admin_taille').hide();
    $('#a2 #receive_hoodie').hide();
    $('#a2 #admin_taille_h').hide();
    switch ($(this).attr('id')) {
        case '50':
            $('#cadeaux').html($('#admin_pibag').html());
            $('#a2 #receive_pibag').show();
            $('#a2 #admin_pibag_id').show();
            break;
        case '100':
            $('#cadeaux').html($('#admin_pishirt').html());
            $('#a2 #receive_pibag').show();
            $('#a2 #admin_pibag_id').show();
            $('#a2 #receive_pishirt').show();
            $('#a2 #admin_taille').show();
            break;
        case '250':
            $('#cadeaux').html($('#admin_hoodie').html());
            $('#a2 #receive_pibag').show();
            $('#a2 #admin_pibag_id').show();
            $('#a2 #receive_pishirt').show();
            $('#a2 #admin_taille').show();
            $('#a2 #receive_hoodie').show();
            $('#a2 #admin_taille_h').show();
            break;
        default:
            $('#cadeaux').html($('#admin_nothing').html());
            $('#a2 #receive_main').hide();
    }
    $('#a2 #contrepartie').val($(this).attr('id'));
    $('#a2 #subform').show();
});
