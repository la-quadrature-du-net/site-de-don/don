function submit_form(form) {
    document.forms[form].submit();
}

function submit_form_confirm(form, confirmation) {
    if (confirm(confirmation)) {
        submit_form(form);
    }
}

function set_field_value(form, field, value) {
    document.forms[form][field].value = value;
}

function valid(id) {
    set_field_value('f', 'action', 'valid');
    set_field_value('f', 'id', id);
    submit_form('f');
}

function invalid(id) {
    set_field_value('f', 'action', 'invalid');
    set_field_value('f', 'id', id);
    submit_form('f');
}

function add(id) {
    set_field_value('ajout', 'id', id);
    submit_form('ajout');
}

function del(id) {
    set_field_value('f', 'action', 'del');
    set_field_value('f', 'id', id);
    submit_form_confirm('f', 'Êtes-vous sûr ?');
}

function sent(id) {
    set_field_value('f', 'action', 'sent');
    set_field_value('f', 'id', id);
    submit_form('f');
}

function asked(id) {
    set_field_value('f', 'action', 'asked');
    set_field_value('f', 'id', id);
    submit_form('f');
}

function npai(id) {
    set_field_value('f', 'action', 'npai');
    set_field_value('f', 'id', id);
    submit_form('f');
}

function canceled(id) {
    set_field_value('f', 'action', 'canceled');
    set_field_value('f', 'id', id);
    submit_form('f');
}

function export_csv() {
    set_field_value('f', 'action', 'export');
    submit_form('f');
    set_field_value('f', 'action', '');
}

function current(id) {
    set_field_value('f', 'action', 'current');
    set_field_value('f', 'id', id);
    submit_form('f');
}

function to_relaunch(id) {
    set_field_value('f', 'action', 'to_relaunch');
    set_field_value('f', 'id', id);
    submit_form('f');
}

function relaunched(id) {
    set_field_value('f', 'action', 'relaunched');
    set_field_value('f', 'id', id);
    submit_form('f');
}

function check_all() {
    checked = document.getElementById('all').checked;
    tags = document.getElementsByTagName('input');
    for (i=0;i<tags.length;i++){
        if (tags[i].id.substr(0,4)=='chk_'){
            tags[i].checked = checked;
        }
    }
}

function checkbox_command(){
    result = '';
    tags = document.getElementsByTagName('input');
    for (i=0;i<tags.length;i++){
        if (tags[i].id.substr(0,4)=='chk_'){
            if (tags[i].checked){
                result += tags[i].id.substr(4) + ', ';
            }
        }
    }
    set_field_value('f', 'checkboxes', result.substr(0,result.length-2));
    set_field_value('f', 'action', 'command');
    submit_form('f');
}

