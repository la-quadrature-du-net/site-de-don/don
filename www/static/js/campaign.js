/*
Javascript utilisé par le site de campagne.
*/

/*
Gestion des dons
*/
var val = 0;

function check_amount() {
    console.log("check amount : " + val);
    $(".cadeaux br ~ p").hide();
    if (val<5) {
        console.log("OK 1");
        $("#no_cado").show();
    } else if (val<100) {
        console.log("OK 4");
        $("#pibag").show();
    } else if (val<250) {
        console.log("OK 5");
        $("#pishirt").show();
    } else {
        console.log("OK 6");
        $("#hoodie").show();
    }

    // Display the image with a full opacity
    if (val >= 50) {
        console.log(val)
        $("#ipibag img").css("opacity", "1");
        $("#ipibag p").css("opacity", "0");
    }
    if (val >= 100) {
        console.log(val)
        $("#ipishirt img").css("opacity", "1");
        $("#ipishirt p").css("opacity", "0");
    }
    if (val >= 250) {
        console.log(val)
        $("#ihoodie img").css("opacity", "1");
        $("#ihoodie p").css("opacity", "0");
    }

    // And with an almost transparency when we're going down
    if (val < 50) {
        console.log(val)
        $("#ipibag img").css("opacity", "0.2");
        $("#ipibag p").css("opacity", "1");
    }
    if (val < 100) {
        console.log(val)
        $("#ipishirt img").css("opacity", "0.2");
        $("#ipishirt p").css("opacity", "1");
    }
    if (val < 250) {
        console.log(val)
        $("#ihoodie img").css("opacity", "0.2");
        $("#ihoodie p").css("opacity", "1");
    }

    // Not let's update the number of month
    if (val == 0) {
        console.log(val == 0);
        var pibagmonth = 0;
        var pishirtmonth = 0;
        var hoodiemonth = 0;
    }
    else {
        var pibagmonth = Math.ceil(50/val);
        var pishirtmonth = Math.ceil(100/val);
        var hoodiemonth = Math.ceil(250/val);
    }
    // We want to replace -, Infinity, and NaN as well as numbers
    var pibag = $("#ipibag p").text().replace(/-?(NaN|Infinity|[0-9]+)/, pibagmonth);
    var pishirt = $("#ipishirt p").text().replace(/-?(NaN|Infinity|[0-9]+)/, pishirtmonth);
    var hoodie = $("#ihoodie p").text().replace(/-?(NaN|Infinity|[0-9]+)/, hoodiemonth);
    if (val == 0) {
        $("#ipibag p").hide();
        $("#ipishirt p").hide();
        $("#ihoodie p").hide();
    }
    else {
        $("#ipibag p").show();
        $("#ipishirt p").show();
        $("#ihoodie p").show();
    }
    $("#ipibag p").text(pibag);
    $("#ipishirt p").text(pishirt);
    $("#ihoodie p").text(hoodie);
}

// Clic sur le bouton de don ponctuel
function donate() {
    document.forms['f1'].monthly.value = 0;
    document.forms['f1'].submit();
}
// Clic sur le bouton de don récurrent
function regular() {
    document.forms['f1'].monthly.value = 1;
    document.forms['f1'].submit();
}


// Initialisation de toutes les fonctions globales sur le site
$(function() {
    $("#amounts_holder input").click(function(e){
        if (!isNaN(parseFloat($(this).val())) && isFinite($(this).val()))
        {
            val = $(this).val()-0;
            check_amount();
        }
    });

    $(".othersum").keyup(function(e){
        //console.log('checking amount');
        val = $(this).val()-0;
        check_amount();
    });

    $(".goal .counter").lettering();
    update_load_bar();
});

// Sélection de la somme par défaut
if ($("#amounts_holder input:checked").attr('id')=='sum1')
{
    val = $(".othersum").val()-0;
} else {
    val = $("#amounts_holder input:checked").val()-0;
}
//console.log(val);
check_amount();

/*
Ajout du camembert de budget en utilisant la bibliothèque raphael et sa librairie
de diagrammes g.raphael.
*/
function addBudgetPieChart() {
    var values = [];
    var legends = [];
    var colors = [];
    for (b in budget) {
        legends.push(b);
        values.push(budget[b][0]);
        colors.push(budget[b][1]);
    }
    // Ajout du camembert
    if (window.innerWidth<600) {
        var r = Raphael("budget_pie", 500, 300);
        var budget_pie = r.piechart(110, 110, 80, values , { colors: colors, legend: legends, legendpos: "south", });
    } else {
        var r = Raphael("budget_pie", 700, 200);
        var budget_pie = r.piechart(100, 110, 80, values , { colors: colors, legend: legends, legendpos: "east", });
    }
    var scale = 1.1;
    budget_pie.hover(
        function () {
            this.sector.stop();
            this.sector.scale(scale, scale, this.cx, this.cy);
            if (this.label) {
                this.label[0].stop();
                this.label[0].attr({ r: 7.5 });
                this.label[1].attr({ "font-weight": 800 });
            }
        }, function () {
            this.sector.animate({ transform: 's1 1 ' + this.cx + ' ' + this.cy }, 500, "bounce");
            if (this.label) {
                this.label[0].animate({ r: 5 }, 500, "bounce");
                this.label[1].attr({ "font-weight": 400 });
            }
        }
    );
}

function addFinancePieChart() {
    var values = [];
    var legends = [];
    for (b in finance) {
        legends.push(b);
        values.push(finance[b]);
    }
    // Ajout du camembert
    var r = Raphael("finance_pie", 500, 200);
    var finance_pie = r.piechart(100, 110, 80, values , { legend: legends, legendpos: "east", });
    var scale = 1.1;
    finance_pie.hover(
        function () {
            this.sector.stop();
            this.sector.scale(scale, scale, this.cx, this.cy);
            if (this.label) {
                this.label[0].stop();
                this.label[0].attr({ r: 7.5 });
                this.label[1].attr({ "font-weight": 800 });
            }
        }, function () {
            this.sector.animate({ transform: 's1 1 ' + this.cx + ' ' + this.cy }, 500, "bounce");
            if (this.label) {
                this.label[0].animate({ r: 5 }, 500, "bounce");
                this.label[1].attr({ "font-weight": 400 });
            }
        }
    );

    // Le camembert étant situé dans une zone spécifique, il faut en faire une copie
    $('#finance_pie_fake').html($('#finance_pie').html());
    $('#finance_pie').remove();
}

// Remplissage de la barre de soutien
function update_load_bar(){
    var colors = ["#80151f", "#f05d63", "#e2ca20", "#8abf16", "#47f70d", "#07b1e5"];
    // 60%
    //var colors_2 = ["#97414b", "#f07b81", "#e5d14a", "#9fc842", "#6af73b", "#37bee8"];
    // 50%
    //var colors_2 = ["#b47379", "#fc9c9f", "#efe078", "#bad77c", "#6dcff2", "#69d1f0"];
    // 40%
    var colors_2 = ["#bf8a8f", "#f9aeae", "#f0e48f", "#c3df8b", "#a3fb86", "#85d6f6"];
    var budget = total+manque;
    var pourcent_fix = (total/budget)*80;
    var max_pourcent = 99;
    var pourcent_pledge = ((total + provisionnel_recurrents)/budget)*80;
    if (pourcent_fix>=max_pourcent){
        pourcent_fix = max_pourcent;
        $('#confirmed').css('background-color', colors[5]);
        $('#pledged').css('background-color', colors_2[5]);
        $(".goal span.counter.donor span").css("border-color", colors[5]);
        $(".goal span.counter.recurrents span").css("border-color", colors_2[5]);
    }else if (pourcent_fix>80){
        $('#confirmed').css('background-color', colors[4]);
        $('#pledged').css('background-color', colors_2[4]);
        $(".goal span.counter.donor span").css("border-color", colors[4]);
        $(".goal span.counter.recurrents span").css("border-color", colors_2[4]);
    }else if (pourcent_fix>60){
        $('#confirmed').css('background-color', colors[3]);
        $('#pledged').css('background-color', colors_2[3]);
        $(".goal span.counter.donor span").css("border-color", colors[3]);
        $(".goal span.counter.recurrents span").css("border-color", colors_2[3]);
    }else if (pourcent_fix>40){
        $('#confirmed').css('background-color', colors[2]);
        $('#pledged').css('background-color', colors_2[2]);
        $(".goal span.counter.donor span").css("border-color", colors[2]);
        $(".goal span.counter.recurrents span").css("border-color", colors_2[2]);
    }else if (pourcent_fix>20){
        $('#confirmed').css('background-color', colors[1]);
        $('#pledged').css('background-color', colors_2[1]);
        $(".goal span.counter.donor span").css("border-color", colors[1]);
        $(".goal span.counter.recurrents span").css("border-color", colors_2[1]);
    }else{
        $('#confirmed').css('background-color', colors[0]);
        $('#pledged').css('background-color', colors_2[0]);
        $(".goal span.counter.donor span").css("border-color", colors[0]);
        $(".goal span.counter.recurrents span").css("border-color", colors_2[0]);
    }
    $('#confirmed').css('width', pourcent_fix+'%');
    $('#pledged').css('width', pourcent_pledge+'%');
    /*
    $('.slider .bar').animate(
        {
            width: pourcent+"%"
        },
        {
            duration: 3000,
            easing: "easeOutQuad",
            step: function(now, tween){
                if (now>100){
                    $('.slider .bar').css('background-color', colors[5]);
                }else if (now>80){
                    $('.slider .bar').css('background-color', colors[4]);
                }else if (now>60){
                    $('.slider .bar').css('background-color', colors[3]);
                }else if (now>40){
                    $('.slider .bar').css('background-color', colors[2]);
                }else if (now>20){
                    $('.slider .bar').css('background-color', colors[1]);
                }
            }
        }
    );
    */
}
