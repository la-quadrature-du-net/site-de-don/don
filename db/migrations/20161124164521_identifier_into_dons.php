<?php

use Phinx\Migration\AbstractMigration;

class IdentifierIntoDons extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
	    $dons = $this->table('dons');
	    if (!$dons->hasColumn('identifier')) {
		    $dons->addColumn('identifier', 'string', array('limit' => '20', 'null' => true))
			->save();
	    }
	    $identifiers = $this->table('identifiers');
	    if (!$identifiers->hasColumn('expiration')) {
		    $identifiers->addColumn('expiration', 'date', array('null' => true))
			    ->save();
	    }

	    // Let's get the last 101 payment associated to a user and get its identifier
	    $users = $this->table('users');
	    if ($users->hasColumn('identifier')) {
		    $dons_101 = $this->fetchAll("SELECT dons.id, dons.user_id, users.identifier, dons.datec FROM dons JOIN users ON dons.user_id = users.id WHERE dons.status = 101 AND users.identifier is not null GROUP BY user_id ORDER by dons.datec");
		    // Now for each 101, we need to update all the 101 and 102 of a specific users where datec is
		    // after the 101.
		    foreach ($dons_101 as $don_101) {
			    $dons_recur = $this->fetchAll("SELECT dons.id FROM dons WHERE dons.datec >= '{$don_101[3]}' AND dons.user_id = {$don_101[1]} AND dons.status in (101, 102)");
			    // Update the dons
			    foreach ($dons_recur as $don_recur) {
				    $this->execute("UPDATE dons SET identifier = '{$don_101[2]}' WHERE dons.id = {$don_recur[0]}");
			    }
			    $identifiers->insert(['user_id' => $don_101[1], 'identifier' => $don_101[2]]);
			    $identifiers->saveData();
		    }
		    $users->removeColumn('identifier')
			    ->save();
	    }
    }

}
