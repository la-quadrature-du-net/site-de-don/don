<?php

use Phinx\Migration\AbstractMigration;

class RemovePiplomes extends AbstractMigration
{
    public function change()
    {
        if ($this->hasTable('piplomes')) {
            $this->table('piplomes')->drop()->save();
        }
    }
}
