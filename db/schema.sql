-- MySQL dump 10.16  Distrib 10.1.18-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: soutien
-- ------------------------------------------------------
-- Server version	10.1.18-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `adresses`
--

DROP TABLE IF EXISTS `adresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `adresse` text NOT NULL,
  `adresse2` text,
  `codepostal` int(11) NOT NULL,
  `ville` varchar(255) DEFAULT NULL,
  `etat` varchar(255) DEFAULT NULL,
  `pays` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `defaut` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`,`user_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `adresses_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=259 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bonus`
--

DROP TABLE IF EXISTS `bonus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bonus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `year` int(10) unsigned NOT NULL,
  `quoi` varchar(255) NOT NULL,
  `datas` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10665 DEFAULT CHARSET=utf8 COMMENT='Les bonus obtenus';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contreparties`
--

DROP TABLE IF EXISTS `contreparties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contreparties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datec` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(10) unsigned NOT NULL,
  `quoi` varchar(255) NOT NULL,
  `taille` int(10) unsigned NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `adresse_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `adresse_id` (`adresse_id`),
  CONSTRAINT `contreparties_ibfk_1` FOREIGN KEY (`adresse_id`) REFERENCES `adresses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10557 DEFAULT CHARSET=utf8 COMMENT='Les demandes de contreparties';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dons`
--

DROP TABLE IF EXISTS `dons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(3) unsigned NOT NULL,
  `datec` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `somme` int(10) unsigned NOT NULL,
  `lang` varchar(5) CHARACTER SET latin1 NOT NULL DEFAULT 'en_US',
  `cadeau` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `abo` tinyint(4) NOT NULL DEFAULT 0,
  `taille` int(10) unsigned NOT NULL DEFAULT 0,
  `public` int(10) unsigned NOT NULL DEFAULT 0,
  `pdf` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `decimale` int(10) unsigned NOT NULL DEFAULT 0,
  `datee` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mailsent` tinyint(4) DEFAULT '0',
  `color` varchar(7) CHARACTER SET latin1 DEFAULT NULL,
  `pi_x` int(11) DEFAULT NULL DEFAULT 0,
  `pi_y` int(11) DEFAULT NULL DEFAULT 0,
  `hash` varchar(64) DEFAULT NULL DEFAULT "",
  `taille_h` int(10) unsigned NOT NULL DEFAULT 0,
  `fdnn_user` bigint(20) NOT NULL DEFAULT '0',
  `color_2` varchar(7) DEFAULT '',
  `cumul` int(11) DEFAULT '0',
  `adresse_id` int(11) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pdf` (`pdf`),
  KEY `decimale` (`decimale`),
  KEY `mailsent` (`mailsent`),
  KEY `hash` (`hash`),
  KEY `adresse_id` (`adresse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10851 DEFAULT CHARSET=utf8 COMMENT='Les dons a la quadrature du net';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `identifiers`
--

DROP TABLE IF EXISTS `identifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `identifiers` (
  `user_id` int(10) unsigned DEFAULT NULL,
  `identifier` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `phinxlog`
--

DROP TABLE IF EXISTS `phinxlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '',
  `data` text,
  `ip` varchar(40) DEFAULT NULL,
  `agent` varchar(255) DEFAULT NULL,
  `stamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `hash` varchar(64) CHARACTER SET latin1 NOT NULL,
  `total` int(10) unsigned  NOT NULL DEFAULT 0,
  `cumul` int(10) unsigned NOT NULL DEFAULT 0,
  `pseudo` varchar(255) NOT NULL DEFAULT "",
  `identifier` varchar(50) DEFAULT NULL,
  `expiration` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `commentaire` text DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=15494 DEFAULT CHARSET=utf8 COMMENT='Les donateurs';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-16 15:54:35
