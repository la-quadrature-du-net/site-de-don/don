<?php

use Phinx\Seed\AbstractSeed;

class CounterpartSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = array(
            array(
                'id' => 1,
                'datec' => '2016-06-22 12:34',
                'user_id' => 1,
                'quoi' => 'pishirt', // [pibag|pishirt|hoodie]
                'taille' => 8,
                'status' => 1,
                'adresse_id' => 1,
                'parent' => 1,
            ),
            array(
                'id' => 2,
                'datec' => '2016-06-22 12:34',
                'user_id' => 2,
                'quoi' => 'hoodie', // [pibag|pishirt|hoodie]
                'taille' => 2,
                'status' => 2,
                'adresse_id' => null,
                'parent' => 4,
            ),
            array(
                'id' => 3,
                'datec' => '2016-06-22 12:34',
                'user_id' => 2,
                'quoi' => 'pibag', // [pibag|pishirt|hoodie]
                'taille' => 2,
                'status' => 2,
                'adresse_id' => null,
                'parent' => 4,
            ),
            array(
                'id' => 4,
                'datec' => '2016-06-22 12:34',
                'user_id' => 2,
                'quoi' => 'hoodie', // [pibag|pishirt|hoodie]
                'taille' => 2,
                'status' => 2,
                'adresse_id' => null,
                'parent' => 4,
            ),
        );

        $this->table('contreparties')->insert($data)->save();
    }
}
