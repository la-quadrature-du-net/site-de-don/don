<?php

use Phinx\Seed\AbstractSeed;

class AddressSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = array(
            array(
                'id' => 1,
                'nom' => 'Main',
                'adresse' => '1 rue Ménars',
                'adresse2' => null,
                'codepostal' => 75001,
                'ville' => 'Paris',
                'etat' => null,
                'pays' => 'France',
                'user_id' => 1,
            ),
            array(
                'id' => 2,
                'nom' => 'Bob address',
                'adresse' => '4 rue Ménars',
                'adresse2' => null,
                'codepostal' => 75003,
                'ville' => 'Paris',
                'etat' => null,
                'pays' => 'France',
                'user_id' => 2,
            ),
        );

        $this->table('adresses')->insert($data)->save();
    }
}
