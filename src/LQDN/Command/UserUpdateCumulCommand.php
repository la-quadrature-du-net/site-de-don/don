<?php

namespace LQDN\Command;

class UserUpdateCumulCommand
{
    private $id;
    private $cumul;

    public function __construct($id, $cumul)
    {
        $this->id = $id;
        $this->cumul = $cumul;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCumul()
    {
        return $this->cumul;
    }
}
