<?php

namespace LQDN\Command;

class DonationResetPdfCommand
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
}
