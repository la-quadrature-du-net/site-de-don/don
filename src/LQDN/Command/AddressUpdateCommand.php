<?php

namespace LQDN\Command;

class AddressUpdateCommand
{
    private $addressId;
    private $userId;
    private $name;
    private $address;
    private $address2;
    private $postalCode;
    private $city;
    private $country;
    private $state;

    public function __construct($addressId, $userId, $name, $address, $address2, $postalCode, $city, $country, $state)
    {
        $this->addressId = $addressId;
        $this->userId = $userId;
        $this->name = $name;
        $this->address = $address;
        $this->address2 = $address2;
        $this->postalCode = $postalCode;
        $this->city = $city;
        $this->country = $country;
        $this->state = $state;
    }

    public function getAddressId()
    {
        return $this->addressId;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getAddress2()
    {
        return $this->address2;
    }

    public function getPostalCode()
    {
        return $this->postalCode;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getState()
    {
        return $this->state;
    }
}
