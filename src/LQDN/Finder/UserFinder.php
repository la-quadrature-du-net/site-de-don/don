<?php

namespace LQDN\Finder;

use Doctrine\DBAL\Connection;

class UserFinder
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * findById
     *
     * @param int $id
     *
     * @return []
     */
    public function findById($id)
    {
        return $this->connection->fetchAssoc("SELECT * FROM users WHERE id = :id", ['id' => (int) $id]);
    }

    /**
     * findByEmail
     *
     * @param mixed $email
     *
     * @return []
     */
    public function findByEmail($email)
    {
        return $this->connection->fetchAssoc("SELECT * FROM users WHERE email = :email", ['email' => $email]);
    }

    /**
     * findByPseudo
     *
     * @param mixed pseudo
     *
     * @return []
     */
    public function findByPseudo($pseudo)
    {
        return $this->connection->fetchAssoc("SELECT * FROM users WHERE pseudo = :pseudo", ['pseudo' => $pseudo]);
    }

    /**
     * The search from the admin.
     *
     * @param string $text
     * @param string $status
     * @param int $limit
     *
     * @return array
     */
    public function adminSearch($text, $status, $limit = 50)
    {
        $query = 'SELECT id, email, pseudo, total, cumul, expiration, status, commentaire FROM users WHERE 1 = 1';
        $params = [];

        if ('' !== $text) {
            $query .= " AND (pseudo LIKE :text OR email LIKE :text OR id LIKE :text)";
            $params['text'] = "%$text%";
        }

        if ('' !== $status) {
            $query .= " AND status = :status";
            $params['status'] = $status;
        }

        $query .= " LIMIT $limit";

        return $this->connection->fetchAll($query, $params);
    }
}
