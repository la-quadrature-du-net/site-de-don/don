<?php

namespace LQDN\Finder;

use Doctrine\DBAL\Connection;

class AddressFinder
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Return all adresses of a given user.
     *
     * @param mixed $userId
     *
     * @return array
     */
    public function findByUserId($userId)
    {
        $userId = (int) $userId;
        $stmt = $this->connection->executeQuery('SELECT * FROM adresses WHERE user_id = :user_id', ['user_id' => $userId]);
        $addresse = $stmt->fetch();
        return $addresse;
    }

    /**
     * Return adresses linked to a counterpart
     *
     * @param mixed $counterpartId
     *
     * @return array
     */
    public function findByCounterpartId($counterpartId)
    {
        $counterpartId = (int) $counterpartId;
        $stmt = $this->connection->executeQuery('SELECT * FROM adresses WHERE user_id = :user_id', ['user_id' => $counterpartId]);
        $addresse = $stmt->fetch();
        return $addresse;
    }
}
