<?php

namespace LQDN\Handler;

use Doctrine\DBAL\Connection;
use LQDN\Command\CounterpartCreateCommand;
use LQDN\Command\CounterpartDeleteCommand;
use LQDN\Command\CounterpartChangeStateCommand;
use LQDN\Command\AdminUpdateParentCommand;
use LQDN\Exception\CounterpartAlreadyExistsException;

class CounterpartHandler
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Delete a counterpart
     *
     * @param CounterpartDeleteCommand $command
     */
    public function handleCounterpartDeleteCommand(CounterpartDeleteCommand $command)
    {
        $counterpartId = $command->getCounterpartId();
        // Let's check if the counterpart exist
        $this->connection->executeUpdate(
            "DELETE FROM contreparties WHERE id = :id",
            ['id' => $counterpartId]
        );
    }

    /**
     * Create a counterpart
     *
     * @param CounterpartCreateCommand $command
     */
    public function handleCounterpartCreateCommand(CounterpartCreateCommand $command)
    {
        $userId = $command->getUserId();
        $adressId = $command->getAddressId();

        $query =<<<EOF
INSERT INTO contreparties(datec, user_id, adresse_id, quoi, taille, status, commentaire, parent)
VALUES (:datec, :user_id, :adresse_id, :quoi, :taille, :status, :commentaire, :parent)
EOF;
        $stmt = $this->connection->prepare($query);
        $stmt->bindValue('datec', $command->getDateCreation());
        $stmt->bindValue('user_id', $command->getUserId());
        $stmt->bindValue('adresse_id', $command->getAddressId());
        $stmt->bindValue('quoi', $command->getQuoi());
        $stmt->bindValue('taille', $command->getTaille());
        $stmt->bindValue('status', $command->getStatus());
        $stmt->bindValue('commentaire', $command->getCommentaire());
        $stmt->bindValue('parent', $command->getParent());
        $stmt->execute();
    }

    /**
     * Chnge the state of a counterpart
     *
     * @param CounterpartChangeStateCommand $command
     */
    public function handleCounterpartChangeStateCommand(CounterpartChangeStateCommand $command)
    {
        $counterpartId = $command->getCounterpartId();
        $status = $command->getState();

        $this->connection->executeUpdate(
            "UPDATE contreparties SET status = :status WHERE id=:id",
            [
                'status' => $status,
                'id' => $counterpartId,
            ]
        );
    }

    /**
     * Test if a counterpart exists
     *
     * @param int $counterpartId
     *
     * @return bool
     */
    private function counterpartExists($counterpartId)
    {
        return (bool) $this->connection->fetchColumn(
            "SELECT 1 FROM contreparties WHERE id = :counterpart_id",
            [
                'counterpart_id' => $counterpartId,
            ],
            0
        );
    }

    /**
     * Test if the counterpart can be deleted
     *
     * @param int $countepartId
     *
     * @return bool
     */
    private function counterpartUsed($counterpartId)
    {
        $status = (int) $this->connection->fetchColumn(
            "SELECT status FROM contreparties WHERE id = :id",
            [
                'id' => $counterpartId,
            ],
            0
        );
        return ($status == 2); // status 2 is delivered counterparts
    }

    /**
     * We try to guess how to group all counterparts, if they're created on the same day, they've been asked
     * as a group of counterparts, so we should parent them together.
     *
     * @param AdminUpdateParentCommand $command
     */
    public function handleAdminUpdateParentCommand(AdminUpdateParentCommand $command)
    {
        // Let's get users first
        $user_ids = $this->connection->executeQuery('SELECT id FROM users')->fetchAll(\PDO::FETCH_COLUMN);

        foreach ($user_ids as $user_id) {
            $user_id =(int) $user_id;

            $parents = $this->connection->executeQuery(
                "SELECT id FROM contreparties c JOIN (SELECT MAX(FIELD(quoi,'pibag', 'pishirt', 'hoodie')) m FROM contreparties GROUP BY user_id, dayofyear(datec)) AS m WHERE FIELD(c.quoi, 'pibag', 'pishirt', 'hoodie') = m.m AND user_id = :user_id",
                [ 'user_id' => $user_id ]
            )->fetchAll(\PDO::FETCH_COLUMN);

            foreach ($parents as $parent) {
                $parent = (int) $parent;

                // Let's get the date of this one
                $day = $this->connection->executeQuery(
                    'SELECT DAYOFYEAR(datec) FROM contreparties WHERE id = :parent',
                    ['parent' => $parent]
                )->fetchAll(\PDO::FETCH_COLUMN)[0];

                // Let's update the parent
                $this->connection->executeUpdate(
                    'UPDATE contreparties SET parent = :parent WHERE DAYOFYEAR(datec) = :day AND user_id = :user_id',
                    ['parent' => $parent, 'day' => (int) $day, 'user_id' => $user_id]
                );
            }
        }
    }
}
